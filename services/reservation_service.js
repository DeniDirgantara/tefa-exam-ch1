const Joi = require('joi');
const SQLNoRow = require('../exceptions/sql_no_row');
const NotFoundError = require('../exceptions/not_found_error');
const InternalServerError = require('../exceptions/internal_server_error');
const NotImplemented = require('../exceptions/not_implemented');
const BadRequest = require('../exceptions/bad_request');
const ReservationModel = require('../models/reservation_model');

class ReservationService {
  constructor(promiseMysqlPool) {
    this.dbPool = promiseMysqlPool;
  }

  async getReservation(id) {
    try {
      const connection = await this.dbPool.getConnection();

      const queryResult = await connection.execute('SELECT id, nama, tanggal, keterangan, createdAt, updatedAt FROM reservation WHERE id = ?', [id]);
      if (queryResult[0].length < 1) {
        throw new SQLNoRow();
      }

      connection.release();

      return queryResult[0][0]

    } catch (err) {
      console.error(err.message);

      let error;

      if (err instanceof SQLNoRow) {
        error = new NotFoundError('reservation is not found');
      } else {
        error = new InternalServerError('an error occurred while getting reservation');
      }

      throw error;
    }
  }

  async getManyReservation() {

    try {
      const connection = await this.dbPool.getConnection();

      const queryResult = await connection.execute('SELECT id, nama, tanggal, keterangan, createdAt, updatedAt FROM reservation');

      if (queryResult[0].length < 1) {
        throw new NotFoundError('reservation is not found');
      }

      connection.release();

      return queryResult[0]

    } catch (err) {
      console.error(err.message);

      let error;

      if (err instanceof NotFoundError) {
        error = new NotFoundError('reservation is not found');
      } else {
        error = new InternalServerError('an error occurred while getting reservation');
      }

      throw error;
    }
  }

  async createReservation(params) {
    try {
     
      // validate the request body of new reservation.
      await ReservationModel.getCreateReservationModel().validateAsync(params);
      
      // construct an reservation entity, it would be the object that used to store to database.
      const reservation = {
        id: null,
        nama: params.nama,
        tanggal: params.tanggal,
        keterangan: params.keterangan,
        createdAt: new Date(),
        updatedAt: null,
      };

      // get db connection.
      const connection = await this.dbPool.getConnection();

      // execute query, it will run the command to store reservation object to db.
      const queryResult = await connection.execute('INSERT INTO reservation SET nama = ?, tanggal = ?, keterangan = ?, createdAt = ?', [
        reservation.nama, reservation.tanggal, reservation.keterangan, reservation.createdAt
      ]);

      // release the db connection, it will send the unused connection back to the pool.
      connection.release();

      // override the reservation id with the new id that returned in query result.
      reservation.id = queryResult[0].insertId;
      
      // return the resolved object that could be the demanded data result.
      return reservation;

    } catch (err) {
      // this block will collect the errors if occurred.

      console.error(err.message);

      if (err instanceof Joi.ValidationError) {
        throw new BadRequest(err.message);
      }

      throw new InternalServerError('an error occurred while getting reservation');
    }
  }

  async updateReservation(id, params) {
    try {

      await ReservationModel.getUpdateReservationModel().validateAsync(params);

      const updatedReservation = {
        id: null,
        nama: params.nama,
        tanggal: params.tanggal,
        keterangan: params.keterangan,
        createdAt: null,
        updatedAt: new Date()
      };

      const connection = await this.dbPool.getConnection();

      const queryResult = await connection.execute(`SELECT * FROM reservation WHERE id = ${id}`);

      if(queryResult[0].length==0){
        throw new NotImplemented('update reservation is not implemented');
      }

      await connection.execute(`update reservation SET nama = ?, tanggal = ?, keterangan = ?, updatedAt = ? where id = ${id}`,[
        updatedReservation.nama, updatedReservation.tanggal, updatedReservation.keterangan, updatedReservation.updatedAt
      ]);
      connection.release();
      return updatedReservation;

      
    } catch (error) {
      console.error(error.message);
      
    }
  
  }

  async deleteReservation(id) {
    try { 
      const connection = await this.dbPool.getConnection();
      const queryResult = await connection.execute(`SELECT * FROM reservation WHERE id = ${id}`);
      if(queryResult[0].length==0){
        throw new NotImplemented('delete reservation is not implemented');
      }
      const deletedReservation = await connection.execute(`DELETE from reservation where id =  ${id}`);
      connection.release();
      return deletedReservation;

    } catch (err) {
      console.error(err.message);
    }
  }
}

module.exports = ReservationService;
