const response = require('../responses/responses');

class ReservationHandler {
  constructor(service) {
    this.service = service;
  }

  async getReservation(req, res) {
    try {
      const result = await this.service.getReservation(req.params.id);
      response.success(res, 200, 'OK', 'detail of a reservation', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }

  async getManyReservation(req, res) {
    try {
      const result = await this.service.getManyReservation();
      response.success(res, 200, 'OK', 'bunch of reservation', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }

  async createReservation(req, res) {
    try {
      const result = await this.service.createReservation(req.body);
      response.success(res, 201, 'CREATED', 'an reservation has successfuly created', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }

  async updateReservation(req, res) {
    const id = req.params.id;
    const update = req.body;

    try {
      const result = await this.service.updateReservation(id, update);
      response.success(res, 200, 'MODIFIED', 'an reservation has successfuly modified', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }

  async deleteReservation(req, res) {
    const id = req.params.id;

    try {
      const result = await this.service.deleteReservation(id);
      response.success(res, 200, 'DELETED', 'an reservation has successfuly deleted', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }
}

module.exports = ReservationHandler;
