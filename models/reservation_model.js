const joi = require('joi');

const getCreateReservationModel = function(){
  const model = joi.object({
    nama: joi.string().required(),
    tanggal: joi.date().required(),
    keterangan: joi.string().required()
  });

  return model;
}

const getUpdateReservationModel = function(){
  const model = joi.object({
    nama: joi.string().required(),
    tanggal: joi.date().required(),
    keterangan: joi.string().required()
  });

  return model;
}

module.exports = {
  getCreateReservationModel: getCreateReservationModel,
  getUpdateReservationModel: getUpdateReservationModel
}
