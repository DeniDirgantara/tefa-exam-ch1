require('dotenv').config();
const configs = require('./configs/configs');
const mysql = require('mysql2');
const express = require('express');
const bodyParser = require('body-parser');
const ReservationService = require('./services/reservation_service');
const ReservationHandler = require('./handlers/reservation_handler');

const responses = require('./responses/responses');

// instantiate configs
const appConfig = configs.get('app');
const mysqlConfig = configs.get('mysql');

/**
 * main() will run the application.
 */
async function main() {
  const mysqlPool = mysql.createPool({
    ...mysqlConfig,
    timezone: '+07:00'
  })
  const promiseMysqlPool = mysqlPool.promise();

  const reservationService = new ReservationService(promiseMysqlPool);
  const reservationHandler = new ReservationHandler(reservationService);

  const router = express();
  router.use(bodyParser.urlencoded({ extended: false }));
  router.use(bodyParser.json());

  router.get('/v1/reservations', reservationHandler.getManyReservation.bind(reservationHandler));
  router.post('/v1/reservations', reservationHandler.createReservation.bind(reservationHandler));
  router.get('/v1/reservations/:id', reservationHandler.getReservation.bind(reservationHandler));
  router.put('/v1/reservations/:id', reservationHandler.updateReservation.bind(reservationHandler));
  router.delete('/v1/reservations/:id', reservationHandler.deleteReservation.bind(reservationHandler));
  router.get('/', function (_, res) {
    responses.success(res, 200, 'OK', 'application is running properly', null, null);
  });

  const server = router.listen(appConfig.port, function () {
    console.info('application is running on port', appConfig.port);
  });

  process.on('SIGINT', function () {
    server.close(function (err) {
      if (err) {
        console.error(err.message);
      }
      console.info('server is gracefully shutdown')
    });

    mysqlPool.end(function (err) {
      if (err) {
        console.error(err.message);
      }
      console.info('mysql is gracefully shutdown')
    })

    setTimeout(function () {
      process.exit(0);
    }, 5 * 1000)
  })
}

main();
